See [.gitlab-ci.yml](.gitlab-ci.yml).

You can allow job failure, only push cache on failure, and fail based on yarn's output:
```yaml
prepare-yarn-cache:
  script:
    - yarn install --check-files --frozen-lockfile | tee yarn_log
    - |
      if grep "success Already up-to-date." yarn_log; then
        exit 0    # skip pushing cache
      else
        exit 123  # push cache
      fi
  allow_failure:
    exit_codes: 123
  cache:
    policy: pull-push
    when: on_failure

other-jobs:
  needs: ["prepare-yarn-cache"]
  cache:
    policy: pull
```


Pipeline run times:
- [02:44 - add more dependencies](https://gitlab.com/lukaw3d/gitlab-ci-push-cache-only-if-changed/-/pipelines/464703909)
- [01:30 - rerun](https://gitlab.com/lukaw3d/gitlab-ci-push-cache-only-if-changed/-/pipelines/464704190)
- [02:41 - upgrade xo dependency](https://gitlab.com/lukaw3d/gitlab-ci-push-cache-only-if-changed/-/pipelines/464704699)
- [01:31 - rerun](https://gitlab.com/lukaw3d/gitlab-ci-push-cache-only-if-changed/-/pipelines/464705567)
- [02:15 - rerun with cache push for comparison](https://gitlab.com/lukaw3d/gitlab-ci-push-cache-only-if-changed/-/pipelines/464705567)
- [01:26 - revert](https://gitlab.com/lukaw3d/gitlab-ci-push-cache-only-if-changed/-/pipelines/464707216)
